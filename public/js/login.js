function initApp() {
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function() {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(){
            console.log("login successfully!");
            var user = firebase.auth().currentUser;
            if(user){
                window.location.assign('home.html');
            }else{
                create_alert("error", "can't get user.");
            }
            
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert("error", errorCode+" -> "+errorMessage);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then(function () {
                window.location = "home.html";
            })
            .catch(function (error) {
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnSignUp.addEventListener('click', function() {
        console.log("txtEmail = "+txtEmail.value);
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function() {
            console.log("signin success!");
            console.log("txtEmail = "+txtEmail.value);
            //Analytics.setUserID(userID.value);
            var successType = "success";
            var successMessage = "sign up successfully!!!";
            create_alert(successType, successMessage);
            var userID;
            userID = creatID(txtEmail.value);
            firebase.database().ref("userID/"+userID).set({roomName : self});
            firebase.database().ref("room_name_list/" + userID).set({data:''});
            txtEmail.value = "";
            txtPassword.value = "";
            
        }).catch(function(error) {
            console.log("signup error QQ");
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });
}

// Custom alert
function create_alert(type, message) {
    console.log("type="+type);
    console.log("message="+message);
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

function creatID(email){
    var ID = '';
    var i;
    for (i = 0; i < email.length; i++) {
        if(email[i] === '@'){
            ID += 'a';
        }else if(email[i] === '.'){
            ID += 'dot';
        }else if(email[i] === '_'){
            ID += 'ul';
        }else{
            ID += email[i];
        }
    }
    console.log('id='+ID);
    return ID;
}

window.onload = function() {
    initApp();
};