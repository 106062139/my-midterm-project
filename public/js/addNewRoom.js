var user_email = '';
var userID = '';
var enterRoomName = '';
var reff = '';

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var brand = document.getElementById('Home');
            user_email = user.email;
            userID = creatID(user_email);
            brand.innerHTML = user.email;
            var btnLogOut = document.getElementById('logout-btn');
            btnLogOut.onclick = function() {
              console.log('you are going to log out!');
              firebase.auth().signOut().then(function() {
                window.location.assign('index.html');
                alert('you are log out!');
                var user = firebase.auth().currentUser;
                console.log(user);
              })
            }
        
    });

    //add new chat room-------------------------------
    add_btn = document.getElementById('add_btn');
    newRoomName = document.getElementById('newRoomName');
      
    add_btn.addEventListener('click', function() {
        console.log("newRoomName = " + newRoomName.value);
        if (newRoomName.value != "") {
            userID = creatID(user_email);
            firebase.database().ref("room_name_list/" + userID + newRoomName.value).set({});
            var newRoomRef = firebase.database().ref("userID/" + userID).push();
            newRoomRef.set({
                create: userID,
                createE: user_email,
                roomName : newRoomName.value
            });
            newRoomName.value = "";
        }
    });

    //chat room list---------------------------------------------------------
    var delay = function(r,s){
        return new Promise(function(resolve,reject){
            setTimeout(function(){
                resolve([r,s]);
            },s); 
        });
    };
  
    delay('a',0).then(function(v){
        console.log(v[0],v[1]);   // 顯示 a 0
        return delay('b',2000);   // 延遲一秒之後，告訴後面的函示顯示 b 1000
    }).then(function(v){
        console.log(v[0],v[1]);
    var str_before_user = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_cont = "</p></div></div>\n";
    userID = creatID(user_email);
    console.log('user.Email= '+user_email);
    console.log('userID= '+userID);
    var roomsRef = firebase.database().ref("userID/"+userID);
    console.log('ref= '+"userID/"+userID);
    var total_room = [];
    var first_c = 0;
    var second_c = 0;

    roomsRef.once('value').then(function(snapshot) {
        document.getElementById('room_list').innerHTML = total_room.join('');

        roomsRef.on('child_added', function(data) {
            second_c += 1;
            if (second_c > first_c) {
                var childD = data.val();
                total_room[total_room.length] = str_before_user + "Room</strong><input class='btn btn-light' type='button' value='"+ childD.roomName +"'onclick='enterroom("+'"'+childD.createE +'","'+ childD.create +'","'+ childD.roomName + '"'+")'>create by "+childD.createE+ str_after_cont;
                document.getElementById('room_list').innerHTML = total_room.join('');
                first_c = total_room.length;
            }
        });
    }).catch(e => console.log(e.message));
        return delay('c',2000);
    });
}

function creatID(email){
    var ID = '';
    var i;
    for (i = 0; i < email.length; i++) {
        if(email[i] === '@'){
            ID += 'a';
        }else if(email[i] === '.'){
            ID += 'dot';
        }else if(email[i] === '_'){
            ID += 'ul';
        }else{
            ID += email[i];
        }
    }
    return ID;
}
function enterroom(createE, create, room){
  console.log('room = '+room);
  enterRoomName = room
  var chatroom = document.getElementById('chatroomm');
      chatroom.innerHTML = '<br><h5 class="border-bottom border-gray pb-2 mb-0">Room '+room+'</h5>create by '+ createE +'<div class="media text-muted pt-3"><input type="button"class="btn btn-warning" value="Add Friend'+ "'s"+ ' email" onclick="addFriend()"></div><br><label for="inputEmail" class="sr-only">Email address</label><input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus><br><div id="chatContent"><div class="my-3 p-3 bg-white rounded box-shadow"></div></div><div class="my-3 p-3 bg-white rounded box-shadow"><textarea class="form-control" rows="2" id="comment" style="resize:none"></textarea><div class="media text-muted pt-3"><input type="button" class="btn btn-success" value="Submit" onclick="chat_submit()"></button></div>';
      console.log('createID = ' +create);
      reff = "room_name_list/" + create + enterRoomName;
      console.log('reff = ' +reff);
      chatInRoom(reff);
}

function addFriend(){
    var txtEmail = document.getElementById('inputEmail');
    var friendID;
    friendID = creatID(txtEmail.value);
    var user = firebase.auth().currentUser;
    console.log('useremail = '+user.email);
    if (friendID !== '' && user.email !=='') {
        var newRoomRef = firebase.database().ref("userID/" + friendID).push();
        newRoomRef.set({
            create: userID,
            createE:user.email,
            roomName : enterRoomName
        });
    txtEmail.value = "";
    }
}
function chat_submit(){
    var post_txt = document.getElementById('comment');
    var user = firebase.auth().currentUser;
    console.log('useremail = '+user.email);
    if ((post_txt.value != '') && (reff !== '')) {
        var newpostref = firebase.database().ref(reff).push();
        newpostref.set({
            email: user.email,
            data: post_txt.value
        });
        post_txt.value = "";
    }
}

function chatInRoom(reff){
    var user = firebase.auth().currentUser;
    var str_before_other_username = "<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 '> ";
    var str_before_self_username= "<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 'style='text-align:right;'> ";
    var str_after_content = "</p></div>\n";
    if(reff!==''){
        console.log('reff = ' +reff);
        var chatRef = firebase.database().ref(reff);
        var total_ = [];
        var first_ = 0;
        var second_ = 0;
  
    chatRef.once('value').then(function(snapshot) {
        document.getElementById('chatContent').innerHTML = total_.join('');
  
        chatRef.on('child_added', function(data) {
            second_ += 1;
            if (second_ > first_) {
                var childData = data.val();
                if(childData.email === user.email){
                    total_[total_.length] = str_before_self_username +'<span class="d-block">' +childData.email + "</span><strong class='text-gray-dark' id='self'>" + childData.data +'</strong>'+ str_after_content
                }else{
                    total_[total_.length] = str_before_other_username +'<span class="d-block">' + childData.email + "</span><strong class='text-gray-dark' id='other'>" + childData.data +'</strong>'+ str_after_content
                }
                document.getElementById('chatContent').innerHTML = total_.join('');
                first_ = total_.length;
            }
        });
    }).catch(e => console.log(e.message));
  }
}
window.onload = function() {
    init();
};