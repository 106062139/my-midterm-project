function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var brand = document.getElementById('Home');
            user_email = user.email;
            brand.innerHTML = user.email;
            var btnLogOut = document.getElementById('logout-btn');
            btnLogOut.onclick = function() {
              console.log('you are going to log out!');
              firebase.auth().signOut().then(function() {
                window.location.assign('index.html');
                alert('you are log out!');
                var user = firebase.auth().currentUser;
                console.log(user);
              })
            }
    });
  
    //hall----------------------------------------------------
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
  
    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('com_list').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value
            });
            post_txt.value = "";
        }
    });
    var str_before_other_username = "<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125'>";
    var str_before_self_username= "<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 'style='text-align:right;'>";
    var str_after_content = "</p></div>\n";
  
    var hallRef = firebase.database().ref('com_list');
    var total_c = [];
    var first_ = 0;
    var second_ = 0;
  
    hallRef.once('value')
        .then(function(snapshot) {
            document.getElementById('post_list').innerHTML = total_c.join('');
  
            hallRef.on('child_added', function(data) {
                second_ += 1;
                if (second_ > first_) {
                    var childData = data.val();
                    if(childData.email === user_email){
                        total_c[total_c.length] = str_before_self_username +'<span class="d-block">'+ childData.email + "</span><strong id='self' class='text-gray-dark'>" + childData.data +'</strong>'+ str_after_content
                    }else{
                        total_c[total_c.length] = str_before_other_username +'<span class="d-block">'+ childData.email + "</span><strong id='other' class='text-gray-dark'>" + childData.data +'</strong>'+ str_after_content
                    }
                    document.getElementById('post_list').innerHTML = total_c.join('');
                    first_ = total_c.length;
                }
            });
        })
        .catch(e => console.log(e.message));

  }


  
  window.onload = function() {
    init();
  };