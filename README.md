# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : Chatroom-2139
* Key functions 
    1. 用信箱或google註冊/登入
    2. 建立個人的聊天室
    3. 將聊天室加入一或多個好友來聊天
    4. 可以看到自己的聊天室list
    
* Other functions
    1. 有一公開聊天室(大廳)
    2. 凡登入者皆可於此留言聊天
    3. 聊天內容皆會記錄下來

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

## 作品網址：
https://midterm-project-106062139.web.app

## Components Description (My Chat Room): 
1. 開一個聊天室 : 輸入聊天室名稱後按下Add即可新增聊天室，並會顯示由你的帳號所開創
2. 將朋友加入聊天室 : 按一下聊天室的名稱後會進入聊天室，並在上方email欄輸入好友的信箱，即可將好友加入。對方的聊天室list會自動跑出聊天室的名稱，按下名稱即可進入聊天
3. 聊天紀錄 : 每一個聊天室皆會記錄聊天內容，每次進入時皆會看到歷史紀錄

# Other Functions Description (Hall): 
1. 大廳聊天室 : 所有登入者皆可在大廳聊天發言
2. 聊天紀錄 : 和個人聊天室一樣會留下記錄，並會顯示由哪個帳號發言
